package pl.dabrowski.xyzapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.dabrowski.xyzapi.dto.AlbumDto;
import pl.dabrowski.xyzapi.service.AlbumService;

import java.util.List;

@RestController
@RequestMapping("/api/albums")
public class AlbumRestController {

    @Autowired
    AlbumService albumService;

    @GetMapping
    public List<AlbumDto> getAllAlbums() {
        return albumService.getAllAlbums();
    }

    @GetMapping("/search")
    public List<AlbumDto> findAlbums(@RequestParam(required = false) Integer userId,
                                 @RequestParam(required = false) String title) {


        if (userId != null && title != null) {
            return albumService.getAlbumsByTitleAndUserId(userId,title);
        }
        else if (userId != null && title == null) {
            return albumService.getAlbumsByUserId(userId);
        }
        else if (userId == null && title != null) {
            return albumService.getAlbumsByTitle(title);
        }
        else return albumService.getAllAlbums();

    }

    @GetMapping("/{id}")
    public AlbumDto getAlbumById ( @PathVariable int id){
        return albumService.getAlbumById(id);
    }

    @GetMapping("/{id}/title")
    public String getAlbumTitleById ( @PathVariable int id){
        return albumService.getAlbumById(id).getTitle();
    }

    @GetMapping("/{id}/userId")
    public int getAlbumUserById ( @PathVariable int id){
        return albumService.getAlbumById(id).getUserId();
    }

}

