package pl.dabrowski.xyzapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.dabrowski.xyzapi.component.DatabaseLogger;
import pl.dabrowski.xyzapi.dto.AlbumDto;
import pl.dabrowski.xyzapi.dto.PhotoDto;
import pl.dabrowski.xyzapi.dto.UserDto;
import pl.dabrowski.xyzapi.service.AlbumService;
import pl.dabrowski.xyzapi.service.ImportExportService;
import pl.dabrowski.xyzapi.service.PhotoService;
import pl.dabrowski.xyzapi.service.UserService;

import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.io.IOException;

@RestController
@RequestMapping("/api/admin")
public class AdminRestController {

    @Autowired
    private UserService userService;

    @Autowired
    private AlbumService albumService;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private DatabaseLogger databaseLogger;

    @Autowired
    private ImportExportService importExportService;

    @PostMapping("/albums")
    public void createAlbum(@Valid @RequestBody AlbumDto albumDto) {
        albumService.createAlbum(albumDto);
    }

    @PutMapping("/albums/{id}")
    public void updateAlbum(@Valid @RequestBody AlbumDto albumDto, @PathVariable int id) {
        albumService.updateAlbum(albumDto, id);
    }

    @DeleteMapping("/albums/{id}")
    public void deleteAlbum(@PathVariable int id) {
        albumService.deleteAlbum(id);
    }

    @PostMapping("/photos")
    public void createPhoto(@Valid @RequestBody PhotoDto photoDto) {
        photoService.createPhoto(photoDto);
    }

    @PutMapping("/photos/{id}")
    public void updatePhoto(@Valid @RequestBody PhotoDto photoDto, @PathVariable int id) {
        photoService.updatePhoto(photoDto, id);
    }

    @DeleteMapping("/photos/{id}")
    public void deletePhoto(@PathVariable int id) {
        photoService.deletePhoto(id);
    }

    @PostMapping("/users")
    public void createUser(@Valid @RequestBody UserDto userDto) {
        userService.createUser(userDto);
    }

    @PutMapping("/users/{id}")
    public void updateUser(@Valid @RequestBody UserDto userDto, @PathVariable int id) {
        userService.updateUser(userDto, id);
    }

    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable int id) {
        userService.deleteUser(id);
    }


    @GetMapping("/db/import")
    public String importDatabaseFromFile() {

        try {
            importExportService.importUsersFromFile();
            importExportService.importAlbumsFromFile();
            importExportService.importPhotosFromFile();
            databaseLogger.databaseLog("Database imported from files");

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "Database imported!";
    }

    @GetMapping("/db/export")
    public String exportDatabaseToFile()  {

        try {
            importExportService.exportListToFile(photoService.getAllPhotos(), "photos");
            importExportService.exportListToFile(userService.getAllUsers(),"users");
            importExportService.exportListToFile(albumService.getAllAlbums(),"albums");
            databaseLogger.databaseLog("Database exported to files");

            } catch (FileNotFoundException e) {
            e.printStackTrace();
            } catch (IOException e) {
            e.printStackTrace();
            }
            return "Database exported!";

    }

    @GetMapping("photos/{id}/export")
    public String exportPhotoToFile(@PathVariable int id) {
        try {
            importExportService.exportDtoToFile(photoService.getPhotoById(id),
                    photoService.getPhotoById(id).getId() + "_"
                            + photoService.getPhotoById(id).getTitle());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Photo Id: " + id + " exported!";
    }

    @GetMapping("albums/{id}/export")
    public String exportAlbumToFile(@PathVariable int id) {
        try {
            importExportService.exportDtoToFile(albumService.getAlbumById(id),
                    albumService.getAlbumById(id).getId() + "_"
                            + albumService.getAlbumById(id).getTitle());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Album Id: " + id + " exported!";
    }

    @GetMapping("users/{id}/export")
    public String exportUserToFile(@PathVariable int id) {
        try {
            importExportService.exportDtoToFile(userService.getUserById(id),
                    userService.getUserById(id).getId() + "_"
                    + userService.getUserById(id).getUsername());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "User Id: " + id + " exported!";
    }




}
