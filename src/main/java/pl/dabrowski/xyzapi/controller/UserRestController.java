package pl.dabrowski.xyzapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.dabrowski.xyzapi.dto.AddressDto;
import pl.dabrowski.xyzapi.dto.CompanyDto;
import pl.dabrowski.xyzapi.dto.GeoDto;
import pl.dabrowski.xyzapi.dto.UserDto;
import pl.dabrowski.xyzapi.service.UserService;
import pl.dabrowski.xyzapi.specification.SearchCriteria;
import pl.dabrowski.xyzapi.specification.UserSpecification;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {


    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserDto> getAll () {
        return userService.getAllUsers();
    }


    @GetMapping("/search")
    public List<UserDto> getAll (@RequestParam(required = false) String name,
                                 @RequestParam(required = false) String username) {
        if (name == null && username == null) {
            return userService.getAllUsers();
        }
        else {
            UserSpecification spec1 = new UserSpecification(new SearchCriteria("name", ":", name));
            UserSpecification spec2 =
                    new UserSpecification(new SearchCriteria("username", ":", username));
            List<UserDto> results = userService.getUsersWithSpecification(spec1, spec2);
            return results;
        }

    }

    @GetMapping("/{id}")
    public UserDto getUserById(@PathVariable int id) {
        return userService.getUserById(id);
    }

    @GetMapping("/{id}/address")
    public AddressDto getUserAddressById(@PathVariable int id) {
        return userService.getUserById(id).getAddress();
    }

    @GetMapping("/{id}/address/geo")
    public GeoDto getUserAddressGeoById(@PathVariable int id) {
        return userService.getUserById(id).getAddress().getGeo();
    }

    @GetMapping("/{id}/company")
    public CompanyDto getUserCompanyById(@PathVariable int id) {
        return userService.getUserById(id).getCompany();
    }

    @GetMapping("/{id}/name")
    public String getUserNameById(@PathVariable int id) {
        return userService.getUserById(id).getName();
    }

    @GetMapping("/{id}/username")
    public String getUserUserameById(@PathVariable int id) { return userService.getUserById(id).getUsername();}

    @GetMapping("/{id}/email")
    public String getUserEmailById(@PathVariable int id) {
        return userService.getUserById(id).getEmail();
    }

    @GetMapping("/{id}/website")
    public String getUserWebsiteById(@PathVariable int id) {
        return userService.getUserById(id).getWebsite();
    }

    @GetMapping("/{id}/phone")
    public String getUserPhoneById(@PathVariable int id) {
        return userService.getUserById(id).getPhone();
    }

}
