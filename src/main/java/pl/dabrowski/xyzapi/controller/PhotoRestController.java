package pl.dabrowski.xyzapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.dabrowski.xyzapi.dto.PhotoDto;
import pl.dabrowski.xyzapi.service.PhotoService;

import java.util.*;

@RestController
@RequestMapping("/api/photos")
public class PhotoRestController {

    @Autowired
    private PhotoService photoService;

    @GetMapping
    public List<PhotoDto> getAll() {
        return photoService.getAllPhotos();
    }


    @GetMapping("/search")
    public List<PhotoDto> findAll(@RequestParam(required = false) Integer albumId,
                                 @RequestParam(required = false) String title) {


        if (albumId != null && title != null) {
            return photoService.getPhotosByTitleAndAlbumId(albumId,title);
        }
        else if (albumId != null && title == null) {
            return photoService.getPhotosByAlbumId(albumId);
        }
        else if (albumId == null && title != null) {
            return photoService.getPhotosByTitle(title);
        }
        else return photoService.getAllPhotos();

    }

    @GetMapping("/search/userId")
    public List<PhotoDto> getByUserId(@RequestParam Integer id) {
            return photoService.getPhotosByUserId(id);
        }


    @GetMapping("/{id}")
    public PhotoDto getPhotoById ( @PathVariable int id){
        return photoService.getPhotoById(id);
    }

    @GetMapping("/{id}/title")
    public String getPhotoTitleById ( @PathVariable int id){
        return photoService.getPhotoById(id).getTitle();
    }

    @GetMapping("/{id}/albumId")
    public int getPhotoAlbumIdById ( @PathVariable int id){
        return photoService.getPhotoById(id).getAlbumId();
    }

    @GetMapping("/{id}/url")
    public String getPhotoUrlById ( @PathVariable int id){
        return photoService.getPhotoById(id).getUrl();
    }

    @GetMapping("/{id}/thumbnail")
    public String getPhotoThumbnailById ( @PathVariable int id){
        return photoService.getPhotoById(id).getThumbnailUrl();
    }


}


