package pl.dabrowski.xyzapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import pl.dabrowski.xyzapi.entity.Album;

import java.util.List;
import java.util.Optional;


public interface AlbumRepository extends JpaRepository<Album, Integer>, JpaSpecificationExecutor<Album> {

    Album findAlbumById(int id);
    List<Album> findByTitleContaining(String title);
    List<Album> findByUserId(int userId);
    List<Album> findByUserIdAndTitleContainingIgnoreCase(int userId, String title);

    @Override
    Optional<Album> findById(Integer integer);
}
