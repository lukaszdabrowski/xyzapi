package pl.dabrowski.xyzapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import pl.dabrowski.xyzapi.entity.Photo;

import java.util.List;
import java.util.Optional;

public interface PhotoRepository extends JpaRepository<Photo, Integer>, JpaSpecificationExecutor<Photo> {

    Photo findPhotoById(int id);
    List<Photo> findByAlbumId(int albumId);
    List<Photo> findByTitleContaining (String title);

    List<Photo> findByAlbumIdAndTitleContainingIgnoreCase(int albumId, String title);

    Optional<Photo> findById(int id);
}
