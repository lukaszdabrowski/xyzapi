package pl.dabrowski.xyzapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.dabrowski.xyzapi.entity.Admin;

public interface AdminRepository extends JpaRepository<Admin, Integer> {

    Admin findByUsername(String username);
}
