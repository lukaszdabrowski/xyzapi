package pl.dabrowski.xyzapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.dabrowski.xyzapi.entity.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole,Integer> {

}
