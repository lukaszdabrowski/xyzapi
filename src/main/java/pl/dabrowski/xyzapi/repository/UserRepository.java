package pl.dabrowski.xyzapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import pl.dabrowski.xyzapi.entity.Company;
import pl.dabrowski.xyzapi.entity.User;

import java.util.List;
import java.util.Optional;


public interface UserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {

    User findUserById(int id);

    List<User> findByNameContaining(String name);

    List<User> findByUsernameContaining(String name);

    List<User> findByCompany(Company company);

    Optional<User> findById(int id);
}
