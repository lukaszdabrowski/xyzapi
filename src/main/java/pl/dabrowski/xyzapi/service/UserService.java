package pl.dabrowski.xyzapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import pl.dabrowski.xyzapi.dto.AddressDto;
import pl.dabrowski.xyzapi.dto.CompanyDto;
import pl.dabrowski.xyzapi.dto.GeoDto;
import pl.dabrowski.xyzapi.dto.UserDto;
import pl.dabrowski.xyzapi.entity.Address;
import pl.dabrowski.xyzapi.entity.Company;
import pl.dabrowski.xyzapi.entity.Geo;
import pl.dabrowski.xyzapi.entity.User;
import pl.dabrowski.xyzapi.repository.UserRepository;
import pl.dabrowski.xyzapi.specification.UserSpecification;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;


    public void createUser(UserDto UserDto) throws IllegalArgumentException {
        Optional<User> userOptional = userRepository.findById(UserDto.getId());
        if (userOptional.isPresent()) {
            throw new IllegalArgumentException("User with " + UserDto.getId() + " is already present in the database!");
        }
        else {
            User user = mapDtoToUser(UserDto);
            userRepository.save(user);
        }
    }

    public void updateUser(UserDto UserDto, int userId) {
        Optional<User> userOptional = userRepository.findById(userId);
        if (!userOptional.isPresent()) {
            throw new IllegalArgumentException("User with " + UserDto.getId() + " is not present in the database!");
        }
        else {
            User user = mapDtoToUser(UserDto);
            user.setId(userId);
            userRepository.save(user);
        }
    }

    public void deleteUser(int userId) {
        userRepository.delete(userRepository.findById(userId).orElseThrow(EntityNotFoundException::new));
    }

    public List<UserDto> getUsersWithSpecification(UserSpecification spec1, UserSpecification spec2) {
        List<User> users = userRepository.findAll(Specification.where(spec1).and(spec2));
        List<UserDto> userDtos = mapUserListToDto(users);
        return userDtos;
    }

    public List<UserDto> getAllUsers () {
        List<User> users = userRepository.findAll();
        return mapUserListToDto(users);
    }

    public UserDto getUserById (int id) {
        User user = userRepository.findUserById(id);
        return mapUserToDto(user);
    }

    public List<UserDto> getUserByName (String name) {
        List<User> users = userRepository.findByNameContaining(name);
        return mapUserListToDto(users);
    }

    public List<UserDto> getUserByUsername (String username) {
        List<User> users = userRepository.findByUsernameContaining(username);
        return mapUserListToDto(users);
    }

    public List<UserDto> mapUserListToDto(List<User> users) {
        List<UserDto> userDtos = new ArrayList<>();
        for (User u : users) {
            UserDto userDto = mapUserToDto(u);
            userDtos.add(userDto);
        }
        return userDtos;
    }

    public UserDto mapUserToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setUsername(user.getUsername());
        userDto.setPhone(user.getPhone());
        userDto.setEmail(user.getEmail());
        userDto.setWebsite(user.getWebsite());
        userDto.setCompany(mapCompanyToDto(user.getCompany()));
        userDto.setAddress(mapAddressToDto(user.getAddress()));
        return userDto;
    }

    public CompanyDto mapCompanyToDto (Company company) {
        CompanyDto companyDto = new CompanyDto();
        companyDto.setName(company.getName());
        companyDto.setBs(company.getBs());
        companyDto.setCatchPhrase(company.getCatchPhrase());
        return companyDto;
    }

    public AddressDto mapAddressToDto (Address address) {
        AddressDto addressDto = new AddressDto();
        addressDto.setCity(address.getCity());
        addressDto.setStreet(address.getStreet());
        addressDto.setSuite(address.getSuite());
        addressDto.setZipcode(address.getZipcode());
        addressDto.setGeo(mapGeoToDto(address.getGeo()));
        return addressDto;
    }

    public GeoDto mapGeoToDto (Geo geo) {
        GeoDto geoDto = new GeoDto();
        geoDto.setLat(geo.getLat());
        geoDto.setLng(geo.getLng());
        return geoDto;
    }


    public User mapDtoToUser(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setName(userDto.getName());
        user.setUsername(userDto.getUsername());
        user.setPhone(userDto.getPhone());
        user.setEmail(userDto.getEmail());
        user.setWebsite(userDto.getWebsite());
        user.setCompany(mapDtoToCompany(userDto.getCompany()));
        user.setAddress(mapDtoToAddress(userDto.getAddress()));
        return user;
    }

    public Company mapDtoToCompany (CompanyDto companyDto) {
        Company company = new Company();
        company.setName(companyDto.getName());
        company.setBs(companyDto.getBs());
        company.setCatchPhrase(companyDto.getCatchPhrase());
        return company;
    }

    public Address mapDtoToAddress (AddressDto addressDto) {
        Address address = new Address();
        address.setCity(addressDto.getCity());
        address.setStreet(addressDto.getStreet());
        address.setSuite(addressDto.getSuite());
        address.setZipcode(addressDto.getZipcode());
        address.setGeo(mapDtoToGeo(addressDto.getGeo()));
        return address;
    }


    public Geo mapDtoToGeo (GeoDto geoDto) {
        Geo geo = new Geo();
        geo.setLat(geoDto.getLat());
        geo.setLng(geoDto.getLng());
        return geo;
    }
}
