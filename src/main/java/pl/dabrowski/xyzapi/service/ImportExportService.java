package pl.dabrowski.xyzapi.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import pl.dabrowski.xyzapi.dto.Dto;
import pl.dabrowski.xyzapi.entity.Album;
import pl.dabrowski.xyzapi.entity.Photo;
import pl.dabrowski.xyzapi.entity.User;
import pl.dabrowski.xyzapi.repository.AlbumRepository;
import pl.dabrowski.xyzapi.repository.PhotoRepository;
import pl.dabrowski.xyzapi.repository.UserRepository;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Service
public class ImportExportService {

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private PhotoRepository photoRepository;

    @Autowired
    private UserRepository userRepository;

    private Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();

    @Value("${export.path}")
    private String path;

    public void exportDtoToFile(Dto dto,String filename) throws IOException {
        FileWriter writer = new FileWriter(path + "/" + filename +".json");
        gson.toJson(dto,writer);
        writer.flush();
        writer.close();
    }

    public void exportListToFile(List items,String filename) throws IOException {
        FileWriter writer = new FileWriter(path +"/" + filename+ ".json");
        gson.toJson(items, writer);
        writer.flush();
        writer.close();
    }

    public void importUsersFromFile() throws IOException {
//        FileReader fileReader = new FileReader(path + "/" +filename);
        Resource resource = new ClassPathResource("static/users.json");
        File file = resource.getFile();
        JsonReader reader = new JsonReader(new FileReader(file));
        User[] users = gson.fromJson(reader, User[].class);
        for (User u: users) {
            userRepository.save(u);
        }
        reader.close();

    }

    public void importPhotosFromFile() throws IOException {
        Resource resource = new ClassPathResource("static/photos.json");
        File file = resource.getFile();
        JsonReader reader = new JsonReader(new FileReader(file));
        Photo[] photos = gson.fromJson(reader, Photo[].class);
        for (Photo p: photos) {
            photoRepository.save(p);
        }
        reader.close();
    }

    public void importAlbumsFromFile() throws IOException {
        Resource resource = new ClassPathResource("static/albums.json");
        File file = resource.getFile();
        JsonReader reader = new JsonReader(new FileReader(file));
        Album[] albums = gson.fromJson(reader, Album[].class);
        for (Album a: albums) {
            albumRepository.save(a);
        }
        reader.close();
    }
}
