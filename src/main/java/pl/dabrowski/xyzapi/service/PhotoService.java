package pl.dabrowski.xyzapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dabrowski.xyzapi.dto.PhotoDto;
import pl.dabrowski.xyzapi.entity.Album;
import pl.dabrowski.xyzapi.entity.Photo;
import pl.dabrowski.xyzapi.repository.AlbumRepository;
import pl.dabrowski.xyzapi.repository.PhotoRepository;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PhotoService {

    @Autowired
    private PhotoRepository photoRepository;

    @Autowired
    private AlbumRepository albumRepository;


    public void createPhoto(PhotoDto photoDto) throws IllegalArgumentException {
        Optional<Photo> photoOptional = photoRepository.findById(photoDto.getId());
        if (photoOptional.isPresent()) {
            throw new IllegalArgumentException("Photo with " + photoDto.getId() + " is already present in the database!");
        }
        else {
            Photo photo = mapDtoToPhoto(photoDto);
            photoRepository.save(photo);
        }
    }

    public void updatePhoto(PhotoDto photoDto, int photoId) {
        Optional<Photo> photoOptional = photoRepository.findById(photoId);
        if (!photoOptional.isPresent()) {
            throw new IllegalArgumentException("Photo with " + photoDto.getId() + " is not present in the database!");
        }
        else {
            Photo photo = mapDtoToPhoto(photoDto);
            photo.setId(photoId);
            photoRepository.save(photo);
        }
    }

    public void deletePhoto(int photoId) {
        photoRepository.delete(photoRepository.findById(photoId).orElseThrow(EntityNotFoundException::new));
    }

    public List<PhotoDto> getAllPhotos () {
        List<Photo> photos = photoRepository.findAll();
        return mapPhotoListToDto(photos);
    }

    public PhotoDto getPhotoById (int id) {
        Photo photo = photoRepository.findPhotoById(id);
        return mapPhotoToDto(photo);
    }

    public List<PhotoDto> getPhotosByAlbumId (int albumId) {
        List<Photo> photos = photoRepository.findByAlbumId(albumId);
        return mapPhotoListToDto(photos);
    }

    public List<PhotoDto> getPhotosByTitle (String title) {
        List<Photo> photos = photoRepository.findByTitleContaining(title);
        return mapPhotoListToDto(photos);
    }

    public List<PhotoDto> getPhotosByUserId (int userId) {
        List<PhotoDto> photoDtos = new ArrayList<>();
        List<Album> albums = albumRepository.findByUserId(userId);
        for (Album a: albums) {
            photoDtos.addAll(mapPhotoListToDto(photoRepository.findByAlbumId(a.getId())));
        }
        return photoDtos;
    }

    public List<PhotoDto> getPhotosByTitleAndAlbumId(int albumId, String title) {
        List<PhotoDto> photoDtosByAlbumId = getPhotosByAlbumId(albumId);
        List<PhotoDto> photoDtos = new ArrayList<>();
        for (PhotoDto p: photoDtosByAlbumId) {
            if (p.getTitle().contains(title)) {
                photoDtos.add(p);
            }
        }
        return photoDtos;

    }

    public PhotoDto mapPhotoToDto(Photo photo) {
        PhotoDto dto = new PhotoDto();
        dto.setId(photo.getId());
        dto.setAlbumId(photo.getAlbumId());
        dto.setTitle(photo.getTitle());
        dto.setUrl(photo.getUrl());
        dto.setThumbnailUrl(photo.getThumbnailUrl());
        return dto;
    }

    public Photo mapDtoToPhoto(PhotoDto dto) {
        Photo photo = new Photo();
        photo.setId(dto.getId());
        photo.setAlbumId(dto.getAlbumId());
        photo.setTitle(dto.getTitle());
        photo.setUrl(dto.getUrl());
        photo.setThumbnailUrl(dto.getThumbnailUrl());
        return photo;
    }

    public List<PhotoDto> mapPhotoListToDto(List<Photo> photos) {
        List<PhotoDto> photoDtos = new ArrayList<>();
        for (Photo p: photos) {
            PhotoDto dto = mapPhotoToDto(p);
            photoDtos.add(dto);
        }
        return photoDtos;
    }
}
