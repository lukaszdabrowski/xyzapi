package pl.dabrowski.xyzapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dabrowski.xyzapi.entity.UserRole;
import pl.dabrowski.xyzapi.repository.UserRoleRepository;

@Service
public class UserRoleService {

    @Autowired
    private UserRoleRepository userRoleRepository;

    public void createUserRole(UserRole userRole) {
        userRoleRepository.save(userRole);
    }
}
