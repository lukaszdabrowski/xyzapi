package pl.dabrowski.xyzapi.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dabrowski.xyzapi.dto.AlbumDto;
import pl.dabrowski.xyzapi.entity.Album;
import pl.dabrowski.xyzapi.repository.AlbumRepository;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AlbumService {

    @Autowired
    private AlbumRepository albumRepository;


    public void createAlbum(AlbumDto albumDto) throws IllegalArgumentException {
        Optional<Album> albumOptional = albumRepository.findById(albumDto.getId());
        if (albumOptional.isPresent()) {
            throw new IllegalArgumentException("Album with " + albumDto.getId() + " is already present in the database!");
        }
        else {
            Album album = mapDtoToAlbum(albumDto);
            albumRepository.save(album);
        }
    }

    public void updateAlbum(AlbumDto albumDto, int albumId) {
        Optional<Album> albumOptional = albumRepository.findById(albumId);
        if (!albumOptional.isPresent()) {
            throw new IllegalArgumentException("Album with " + albumDto.getId() + " is not present in the database!");
        }
        else {
            Album album = mapDtoToAlbum(albumDto);
            album.setId(albumId);
            albumRepository.save(album);
        }
    }

    public void deleteAlbum(int albumId) {
        albumRepository.delete(albumRepository.findById(albumId).orElseThrow(EntityNotFoundException::new));
    }

    public List<AlbumDto> getAllAlbums () {
        List<AlbumDto> albumDtos = new ArrayList<>();
        List<Album> albums = albumRepository.findAll();
        return mapPhotoListToDto(albums);
    }

    public AlbumDto getAlbumById (int id) {
        Optional<Album> albumOptional = albumRepository.findById(id);
        Album album = albumOptional.orElseThrow(EntityNotFoundException::new);
        return mapAlbumToDto(album);
    }

    public List<AlbumDto> getAlbumsByUserId (int userId) {
        List<Album> albums = albumRepository.findByUserId(userId);
        return mapPhotoListToDto(albums);
    }

    public List<AlbumDto> getAlbumsByTitle (String title) {
        List<Album> albums = albumRepository.findByTitleContaining(title);
        return mapPhotoListToDto(albums);
    }


    public List<AlbumDto> getAlbumsByTitleAndUserId(int userId, String title) {
        List<AlbumDto> albumDtos = new ArrayList<>();
        List<Album> albums = albumRepository.findByUserIdAndTitleContainingIgnoreCase(userId,title);
        return mapPhotoListToDto(albums);
    }


    public AlbumDto mapAlbumToDto(Album album) {
        AlbumDto dto = new AlbumDto();
        dto.setId(album.getId());
        dto.setUserId(album.getUserId());
        dto.setTitle(album.getTitle());
        return dto;
    }

    public Album mapDtoToAlbum(AlbumDto dto) {
        Album album = new Album();
        album.setId(dto.getId());
        album.setUserId(dto.getUserId());
        album.setTitle(dto.getTitle());
        return album;
    }

    public List<AlbumDto> mapPhotoListToDto(List<Album> albums) {
        List<AlbumDto> albumDtos = new ArrayList<>();
        for (Album a: albums) {
            AlbumDto dto = mapAlbumToDto(a);
            albumDtos.add(dto);
        }
        return albumDtos;
    }

}
