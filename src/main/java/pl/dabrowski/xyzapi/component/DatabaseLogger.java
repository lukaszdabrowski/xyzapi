package pl.dabrowski.xyzapi.component;



import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

@Component
public class DatabaseLogger {

    private Logger logger;
    private FileHandler fileHandler;

    @Value("${export.path}")
    private String path;

    public void databaseLog (String message) {

        logger = Logger.getLogger("Database Logger");


        try {

            fileHandler = new FileHandler(path +"/db.log",true);
            logger.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            logger.info(message);
            fileHandler.close();

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
