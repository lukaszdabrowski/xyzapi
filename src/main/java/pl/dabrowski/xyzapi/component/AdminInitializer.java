package pl.dabrowski.xyzapi.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.dabrowski.xyzapi.controller.AdminRestController;
import pl.dabrowski.xyzapi.entity.Admin;
import pl.dabrowski.xyzapi.entity.UserRole;
import pl.dabrowski.xyzapi.repository.AdminRepository;
import pl.dabrowski.xyzapi.repository.UserRoleRepository;
import pl.dabrowski.xyzapi.service.*;


import java.util.HashSet;
import java.util.Set;


@Component
public class AdminInitializer implements ApplicationListener<ContextRefreshedEvent> {

        @Autowired
        private AdminService adminService;

        @Autowired
        private UserRoleService userRoleService;

        @Override
        public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

//            UserRole role = new UserRole("ADMIN");
//            userRoleService.createUserRole(role);
//            Set<UserRole> roleSet = new HashSet<>();
//            roleSet.add(role);
//            Admin admin = new Admin("admin", "admin", roleSet);
//            adminService.createAdmin(admin);

        }
}
