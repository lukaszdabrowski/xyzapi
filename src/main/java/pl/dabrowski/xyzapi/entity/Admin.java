package pl.dabrowski.xyzapi.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "admin")
public class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String password;
    private String username;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<UserRole> roles;


    public Admin(String password, String username, Set<UserRole> roles) {
        this.password = password;
        this.username = username;
        this.roles = roles;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<UserRole> roles) {
        this.roles = roles;
    }
}
