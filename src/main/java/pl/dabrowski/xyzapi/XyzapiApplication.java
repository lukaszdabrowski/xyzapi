package pl.dabrowski.xyzapi;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class XyzapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(XyzapiApplication.class, args);

    }

}
