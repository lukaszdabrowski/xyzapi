# xyzapi REST API 

**API for accessing Photo Album data**


**About the Application**

This is Spring Boot based RESTful application program interface (API) web service that uses HTTP requests to GET, PUT, POST and DELETE data 
regarding Photos, Photo Albums and Users from the database of this service. Data can be received and sent in JSON format.

**What does the Application do?**

The application realizes a web service that answers to a HTTP request. When you run the application on
your local computer it starts a web server on port 8080. Note that the application will fail to start, if another
application uses this port or you run this project before and did not exit the process.

**Configuring the application**

To run this simply clone this repository and modify file:

`src/main/resources/application.properties`

In the file you will see line:

`export.path=/`

This is path to the folder that application will use for exporting data from database. Please change it 
according to your wish, but please be remainded that folder needs to exist (it will not be created by application).

Lines:

`spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.datasource.password=root
spring.datasource.username=root
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.url=jdbc:h2:mem:testdb
spring.jpa.hibernate.ddl-auto=create`

can be modified with you desired H2 database configuration or replaced with another datasource entirely.

You can also modify classes:

`src/main/java/pl.dabrowski.xyzapi.configuration.SecurityConfiguration`

and

`main/java/pl.dabrowski.xyzapi.component.AdminInitializer`

with credentials that you will use for administration part of API. Default settings are:

`user:    admin
 password: admin`

Initial dummy data to import is located by default in:

`src/main/resources/static`

If you wish to change import target, please modify import methods in class:

`src/main/java/pl.dabrowski.xyzapi.service.ImportExportService`

**Running the application**

If you have installed Java and the JAVA_PATH environment variable is correctly set, you can easily run the
application using its integrated Tomcat server. Just open a terminal (Mac & Linux) or a cmd (Windows) and execute the following command in the root
directory of this repository:

`mvn spring-boot:run`

Once the application has started successfully, you should be able to see the following lines in the log output:

`2019-03-17 14:00:57.354  INFO 43957 --- [           main] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat started on port(s): 8080 (http)
 2019-03-17 14:00:57.362  INFO 43957 --- [           main] pl.dabrowski.xyzapi.XyzapiApplication    : Started XyzapiApplication in 8.083 seconds (JVM running for 9.223)`

You should then be able to open the address http://localhost:8080/api in your web browser and get to use the API.


**Accessing Data endpoints**

**Photos**

To access full list of available photos use path:

`api/photos`

You can access single photo by it's ID using:

`api/photos/{id}`

You can search photos by it's title or Album ID using:

`api/photos/search?title=yourinput`

`api/photos/search?albumID=yourinput`

or combination of those two parameters:

`api/photos/search?albumID=yourinput&title=yourinput`

You can also search photo that belong to certain User by User ID:

`api/photos/search/userId?id=yourinput`

It's also possible to access directly all internal fields of Photo record:

`api/photos/{id}/title`

`api/photos/{id}/albumId`

As well as URL addresses of Photo and it's thumbnail

`api/photos/{id}/url`

`api/photos/{id}/thumbnail`

All of the above will be displayed as String

**Albums**

To access full list of available albums use path:

`api/albums`

You can access single album by it's ID using:

`api/albums/{id}`

You can search photos by it's title or User ID using:

`api/albums/search?title=yourinput`

`api/photos/search?userId=yourinput`

or combination of those two parameters:

`api/albums/search?userID=yourinput&title=yourinput`


It's also possible to access directly all internal fields of Photo record:

`api/albums/{id}/title`

`api/albums/{id}/userId`


All of the above will be displayed as String

**Users**

To access full list of available users use path:

`api/users`

You can access single photo by it's ID using:

`api/users/{id}`

You can search photos by it's name or username using:

`api/users/search?name=yourinput`

`api/users/search?username=yourinput`

or combination of those two parameters:

`api/photos/search?name=yourinput&usernam=yourinput`


It's also possible to access all internal fields of Photo record:

`api/photos/{id}/name`

`api/photos/{id}/username`

`api/photos/{id}/email`

`api/photos/{id}/phone`

`api/photos/{id}/website`

(all of the above will be displayed as String)

As well as company, address and geolocation of the address which will be displayed in JSON format:

`api/photos/{id}/company`

`api/photos/{id}/address`

`api/photos/{id}/address/geo`


**Administration API**

Application provides CRUD type API for data administration. It can be accessed at:

`api/admin`

This part of application is protected using Spring Security, can be accessed using Admin account with credentials defined 
in SecurityConfiguration class and offers options to add, update and remove users, albums and photos. 

To add record you need to input record type in the url path. For example:

`api/admin/users`

And use HTTP method POST to add record to the database.

To remove record you need to input record type followed by its ID in the url path. For example:

`api/admin/users/{id}`

And use HTTP method DELETE to remove record from the database.

To modify existing record you need to input record type followed by its ID in the url path. For example:

`api/admin/users/{id}`

And use HTTP method PUT to modify record from the database.

Using Administration API you can also import and export data to and from running database.
To perform import of file into database you need to access following path:

`api/admin/import`

Accessing it will automatically begin importing data from defined in application.properties and ImportExportController
files (one for each data type that is handled: users, photos, albums). After import you will receive output
message:

`Database Imported!`

Similarly to perform export of database to files you need to access following path:

`api/admin/export`

Accessing it will automatically begin exporting data to defined in application.properties and ImportExportController
files (one for each data type that is handled: users, photos, albums). After export you also will receive output
message:

`Database Exported!`

You can also export single database record to file using record type followed by ID and by word export, like:

`photos/{id}/export`

This will export the record to a file created in the folder defined in application.properties and ImportExportController
path that will be named:

`{id}_{title of record}.json`

**Enjoy using this service!**


